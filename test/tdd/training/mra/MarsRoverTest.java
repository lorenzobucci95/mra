package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testShouldContaintObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("oi_2,oi_1");
		MarsRover marsRover = new MarsRover(2, 2, planetObstacles);
		assertEquals(true, marsRover.planetContainsObstacleAt(2, 1));
	}
	
	@Test
	public void testExecuteCommandN() throws MarsRoverException {
		//"(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)"
		String roversStatus = "(0,1,N)(o1_0,o1_1)";	
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("oi_0,oi_1");
		MarsRover marsRover = new MarsRover(3,3, planetObstacles);
		assertEquals(roversStatus,marsRover.executeCommand("f"));
	}
	
}
