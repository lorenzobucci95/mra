package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	private int planetX;
	private int planetY;
	List<String> planetObstacles = new ArrayList<>();
	private int x;
	private int y;
	private String dir;
	
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.x = 0;
		this.y = 0;
		this.dir = "N";
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles=planetObstacles;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {

		for(int i=0;i<planetObstacles.size();i++) {
			if((planetObstacles.get(i).charAt(3)==x+48) && (planetObstacles.get(i).charAt(8)==y+48)){
				return true;
			}
		}
		return false;
		
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		//String roversStatus = "("+ x + "," + y + "," + dir;
		//"(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)"
		String roversStatus="";
		int countObstacles=0;
				
		if(dir=="N") {
			if(commandString=="f") {
				y++;
				if(planetContainsObstacleAt(x, y)) {
					countObstacles++;
					roversStatus += "(o" + countObstacles + "_" + x + ",o" +countObstacles+ "_" + y +")";
				}
			} else if(commandString=="b") {
				y--;
				if(planetContainsObstacleAt(x, y)) {
					countObstacles++;
					roversStatus += "(o" + countObstacles + "_" + x + ",o" +countObstacles+ "_" + y +")";
				}
			} else if(commandString=="r") {
				x++;
				if(planetContainsObstacleAt(x, y)) {
					countObstacles++;
					roversStatus += "(o" + countObstacles + "_" + x + ",o" +countObstacles+ "_" + y +")";
				}
			}
		}
	
		return "("+x+","+y+","+dir+")"+roversStatus;
	}
	
}
